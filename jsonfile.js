import axios from 'axios'

const axiosAPI = axios.create({
    baseURL:`http://127.0.0.1:8000/api`,
    headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9yZWdpc3RlciIsImlhdCI6MTU5MDEyMzExOSwiZXhwIjoxNTkyNzE1MTE5LCJuYmYiOjE1OTAxMjMxMTksImp0aSI6Ikl3eFROWW1Ha1IwNkZ2WXoiLCJzdWIiOjIsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.6faEpV1fKCMCNd8Nj6EdDcPyNkFoH3ZTpMgqZ3Qzlr8",
      },
    });



export default axiosAPI;
